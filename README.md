# OpenPOWER Firmware Build Environment

The OpenPOWER firmware build process uses Buildroot to create a toolchain and
build the various components of the PNOR firmware, including Hostboot, Skiboot,
OCC, Petitboot etc.

## Documentation

https://open-power.github.io/op-build/

See the doc/ directory for documentation source. Contributions
are *VERY* welcome!

## Development

Issues, Milestones, pull requests and code hosting is on GitHub:
https://github.com/open-power/op-build

See [CONTRIBUTING.md](CONTRIBUTING.md) for howto contribute code.

* Mailing list: openpower-firmware@lists.ozlabs.org
* Info/Subscribe: https://lists.ozlabs.org/listinfo/openpower-firmware  
* Archives: https://lists.ozlabs.org/pipermail/openpower-firmware/

## Building an image

To build an image for a Talos system:

```
git clone --recursive https://github.com/open-power/op-build.git
cd op-build
./op-build talos_defconfig && ./op-build
```

There are also default configurations for other platforms in
`openpower/configs/`. Current POWER9 platforms include Blackbird and Talos II.

Buildroot/op-build supports both native and cross-compilation - it will
automatically download and build an appropriate toolchain as part of the build
process, so you don't need to worry about setting up a
cross-compiler.  Compiling from an OpenPOWER host is officially supported.

The machine your building on will need Python 2.7, GCC 8.4 (or later), and
a handful of other packages (see below).

### Dependencies for 64-bit little endian Debian systems

1. Install Debian 12 ppc64le.

2. Install the packages necessary for the build:

        sudo apt-get install cscope universal-ctags libz-dev libexpat-dev \
          python2 python-is-python3 language-pack-en texinfo gawk cpio xxd \
          build-essential g++ git bison flex unzip \
          libssl-dev libxml-simple-perl libxml-sax-perl libxml-parser-perl libxml2-dev libxml2-utils xsltproc \
          wget bc rsync
